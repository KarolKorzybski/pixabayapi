package com.example.pixabayapi.model

class Results {
    private var totalHits : Int? = null
    var hits: List<Hits>? = null
        get() = field
        set(value) {
            field = value
        }

}