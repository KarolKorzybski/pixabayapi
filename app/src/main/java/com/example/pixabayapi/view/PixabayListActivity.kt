package com.example.pixabayapi.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pixabayapi.R
import com.example.pixabayapi.adapter.PixabayAdapter
import com.example.pixabayapi.database.AppDatabase
import com.example.pixabayapi.databinding.PixabayBinding
import com.example.pixabayapi.model.Hits
import com.example.pixabayapi.model.Results
import com.example.pixabayapi.rest.RestSingletonPixabay
import com.example.pixabayapi.viewmodel.PixabayViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableMaybeObserver
import io.reactivex.schedulers.Schedulers
import java.util.*
class PixabayListActivity : AppCompatActivity() {

    private var results: List<Results>? = null
    private var disposable: Disposable? = null
    private var binding: PixabayBinding? = null
    private var adapter: PixabayAdapter? = null
    private var viewModel: PixabayViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    private fun init() {
        disposable = CompositeDisposable()
        results = LinkedList()
        initBinding()
    }

    private fun initBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding?.lifecycleOwner = this
        binding?.viewmodel = viewModel
        initViewModel()
        initSearchView()
    }
    private fun saveToDatabase(hits: Hits){
        AppDatabase
            .getInstance(this)
            .hitDao()
            .setHits(hits)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object: DisposableCompletableObserver() {
                override fun onComplete() {
                    Log.d("saveToDabase","onComplete")
                }

                override fun onError(e: Throwable) {
                    Log.d("saveToDabase","onError")
                }

            })
    }
    private fun getIndex() : Long{
        return System.currentTimeMillis()
    }
    private fun getWithDatabase(favoriteViewModel : PixabayViewModel?){
        AppDatabase
            .getInstance(this)
            .hitDao()
            .getAll()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object: DisposableMaybeObserver<List<Hits>>() {
                override fun onSuccess(hitsList: List<Hits>) {
                    val hits = viewModel!!.getViewModel(hitsList)
                    if(favoriteViewModel == null){
                        setBinding(hits)
                        viewModel?.setFavoriteList(hits)
                    }else
                    {
                        val iterator = hits.listIterator()
                        for (item in iterator) {
                            if(favoriteViewModel.id == item.id){
                                viewModel?.setFavoriteList(hits)
                                return
                            }
                        }
                        hits.add(favoriteViewModel)
                        viewModel?.setFavoriteList(hits)
                        saveToDatabase(viewModel!!.getHits(favoriteViewModel))
                    }
                }

                override fun onComplete() {
                    Log.d("getWithDatabase","onComplete")
                }

                override fun onError(e: Throwable) {
                    Log.d("getWithDatabase","onError")
                }

            })
    }
    private fun deleteFromDatabase(deleteViewModel: PixabayViewModel){
        viewModel?.getHits(deleteViewModel)?.id?.let {
            AppDatabase
                .getInstance(this)
                .hitDao()
                .deleteHits(it)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object:DisposableCompletableObserver(){
                    override fun onComplete() {
                        getWithDatabase(null)
                    }

                    override fun onError(e: Throwable) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
        }
    }

    private fun initViewModel() {
        viewModel = PixabayViewModel()
        viewModel = ViewModelProvider(this).get(PixabayViewModel::class.java)
        viewModel?.getLiveData()?.observe(this, androidx.lifecycle.Observer { list -> setBinding(list) })
        viewModel?.getFavoriteViewModel()?.observe(this,androidx.lifecycle.Observer {
                viewModelFavorite ->
                    run {
                        if (viewModelFavorite != null){
                            if(viewModelFavorite.star.get())
                            {
                                getWithDatabase(viewModelFavorite)
                            }
                            else {
                                deleteFromDatabase(viewModelFavorite)
                            }
                        }
                    }
        })
        if(viewModel?.oldText.equals("")) getWithDatabase(null)
    }

    private fun setBinding(results: List<PixabayViewModel>) {
        adapter = viewModel?.let { PixabayAdapter(results, it) }
        binding?.recyclerView?.layoutManager = LinearLayoutManager(this)
        binding?.recyclerView?.adapter = adapter
        adapter?.notifyDataSetChanged()
    }

    private fun initSearchView() {

        binding?.search?.isActivated = true
        binding?.search?.queryHint = "Type your keyword here"
//        binding?.search??.onActionViewExpanded()
        binding?.search?.isIconified = false
        binding?.search?.clearFocus()
        binding?.search?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isNotEmpty() && newText != viewModel?.oldText) {
                    searchMovie(newText)
                }
                else if(newText.isEmpty()){
                    getWithDatabase(null)
                }
                viewModel?.oldText = newText
                return false
            }
        })
    }

    private fun searchMovie(newText: String) {
        disposable = RestSingletonPixabay
            .create()
            ?.getResults(newText, "photo")
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                { results ->
                    run {
                        println("Received: $results")
                        val hits = viewModel!!.getViewModel(results.hits!!)
                        setBinding(hits)
                        viewModel?.viewModels = hits
                    }
                },      // onNext
                { error -> println("Error: $error") },         // onError
                { println("Completed") }                       // onComplete
            )
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }
}
