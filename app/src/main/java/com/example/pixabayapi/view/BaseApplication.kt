package com.example.pixabayapi.view

import android.app.Application
import com.example.pixabayapi.di.components.AppComponent
//import com.example.pixabayapi.di.components.DaggerAppComponent

public class BaseApplication : Application() {
    private var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
//        appComponent = DaggerAppComponent.create()
    }

    fun getAppComponent(): AppComponent? {
        return appComponent
    }
}