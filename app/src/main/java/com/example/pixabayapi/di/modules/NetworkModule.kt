package com.example.pixabayapi.di.modules

import com.example.pixabayapi.rest.ApiPixabay
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyManagementException
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
object NetworkModule {
    private val URL = "https://pixabay.com"
    @Provides
    @Singleton
    internal fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(URL)
            .client(okHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    internal fun provideUserService(retrofit: Retrofit): ApiPixabay {
        return retrofit.create(ApiPixabay::class.java)
    }

    private fun okHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        var client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return client
    }

}