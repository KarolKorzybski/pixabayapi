package com.example.pixabayapi.di.modules

import androidx.lifecycle.ViewModel
import com.example.pixabayapi.di.ViewModelKey
import com.example.pixabayapi.viewmodel.PixabayViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(PixabayViewModel::class)
    internal abstract fun bindViewModel(viewModel: PixabayViewModel): ViewModel
}