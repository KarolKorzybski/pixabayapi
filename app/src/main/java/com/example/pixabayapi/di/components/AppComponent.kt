package com.example.pixabayapi.di.components

import com.example.pixabayapi.di.modules.ContextModule
import com.example.pixabayapi.di.modules.NetworkModule
import com.example.pixabayapi.view.PixabayListActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, ContextModule::class])
interface AppComponent {
    fun inject(activity: PixabayListActivity)
}