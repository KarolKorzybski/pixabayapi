package com.example.pixabayapi.repository

import com.example.pixabayapi.model.Results
import com.example.pixabayapi.rest.ApiPixabay
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class UserRepository @Inject
constructor(private val api: ApiPixabay) {

    fun modelGetResults(s : String): Observable<Results> {
        return api.getResults(s,"photo")
    }
}