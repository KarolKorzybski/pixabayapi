package com.example.pixabayapi.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.pixabayapi.model.Hits

@Database(entities = [Hits::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun hitDao(): HitDao

    companion object{
        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase{
            if (INSTANCE == null){
                INSTANCE = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "DatabaseHits")
                    .build()
            }

            return INSTANCE as AppDatabase
        }
    }
}