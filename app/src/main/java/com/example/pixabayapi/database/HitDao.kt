package com.example.pixabayapi.database

import androidx.room.*
import com.example.pixabayapi.model.Hits
import io.reactivex.Completable
import io.reactivex.Maybe
@Dao
interface HitDao {

    @Query("SELECT * FROM hits")
    abstract fun getAll(): Maybe<List<Hits>>

    //
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setAllHits(hits: List<Hits>): Completable
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setHits(hit: Hits): Completable

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAllHits(results: List<Hits>): Completable

    @Query("DELETE FROM hits WHERE id = :deleteID")
    fun deleteHits(deleteID: Int): Completable
//    @Delete
//    fun deleteHits(hits: Hits) : Completable

    @Query("DELETE FROM hits")
    fun deleteAll(): Completable
}