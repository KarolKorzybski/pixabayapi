package com.example.pixabayapi.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.pixabayapi.R
import com.example.pixabayapi.callback.CallbackList
import com.example.pixabayapi.databinding.AdapterRow
import com.example.pixabayapi.viewmodel.PixabayViewModel

class PixabayAdapter(results: List<PixabayViewModel>, viewModel: PixabayViewModel) :
    RecyclerView.Adapter<PixabayAdapter.PixabayView>(), CallbackList {
    override fun clickStar(favoriteViewModel: PixabayViewModel) {
        favoriteViewModel.star.set(!favoriteViewModel.star.get())
        this.viewModel?.setFavoriteViewModel(favoriteViewModel)
        arrayList?.contains(favoriteViewModel)
        notifyDataSetChanged()
    }

    private var arrayList: List<PixabayViewModel>? = null
    private var viewModel: PixabayViewModel? = null
    private var layoutInflater: LayoutInflater? = null
    //    private var valueFilter: ValueFilter? = null
    private var mData: List<String>? = null
    private var mStringFilterList: List<String>? = null

    init {
        this.arrayList = results
        this.viewModel = viewModel
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): PixabayView {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(viewGroup.context)
        }

        if(this.viewModel !=null && arrayList!=null && arrayList!!.isNotEmpty() && this.viewModel!!.isFavoriteModel(
                arrayList!![i])) {
            this.arrayList!![i].star.set(true)
        }
        val adapterRow : AdapterRow = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater!!,
            R.layout.row_photo,
            viewGroup,
            false
        ) as AdapterRow
        adapterRow.callback = this
        return PixabayView(adapterRow)
    }


    override fun getItemCount(): Int {
        return if (arrayList == null) 0 else arrayList!!.size
    }

    override fun onBindViewHolder(viewHolder: PixabayView, position: Int) {

        val result = arrayList!![position]
        viewHolder.bind(result)
    }

    class PixabayView(private var adapterRow: AdapterRow) :
        RecyclerView.ViewHolder(adapterRow.root) {

        fun bind(result: PixabayViewModel) {
            this.adapterRow.viewmodel = result
            adapterRow.executePendingBindings()
        }

    }
}