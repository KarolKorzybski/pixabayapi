package com.example.pixabayapi.viewmodel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.example.pixabayapi.model.Hits
import com.example.pixabayapi.other.GlideApp
import com.example.pixabayapi.repository.UserRepository
import java.util.*
import javax.inject.Inject

class PixabayViewModel : ViewModel {
    private var favoriteList = LinkedList<PixabayViewModel>()
    var progressVisible: Boolean = true
    var id: Int? = null
    var index: Long = 0
    var star: ObservableBoolean = ObservableBoolean(false)
    private var favoriteViewModel = MutableLiveData<PixabayViewModel>()
    var oldText : String = ""
        get() = field
        set(value) {
            field = value
        }
    var viewModels = LinkedList<PixabayViewModel>()
        get() = field
        set(value) {
            field = value
        }
    var liveData = MutableLiveData<List<PixabayViewModel>>()
    var likes: Int = 0
    var largeImageURL: String = ""
    var user: String = ""
    private lateinit var userRepository: UserRepository;

    fun getUserRepository(): UserRepository {
        return userRepository
    }

    fun setUserRepository(userRepository: UserRepository) {
        this.userRepository = userRepository
    }
    fun getFavoriteList(): LinkedList<PixabayViewModel>{
        return favoriteList
    }
    fun isFavoriteModel(model : PixabayViewModel) : Boolean{
        if(favoriteList.isEmpty()) return false
        val iterator = favoriteList.listIterator()
        for (item in iterator){
            if(model.id == item.id){
                return item.star.get()
            }
        }
        return false
    }

    fun setFavoriteList(favoriteList: LinkedList<PixabayViewModel>) {
        this.favoriteList = favoriteList
    }

    @Inject
    constructor(userRepository: UserRepository) {
        this.userRepository = userRepository
    }
    constructor(hits: Hits) {
        this.likes = hits.likes!!
        this.largeImageURL = hits.webformatURL!!
        this.user = hits.user!!
        this.star.set(hits.isStar)
        this.index = hits.index
        this.id = hits.id
    }
    fun setLiveData(live : List<PixabayViewModel>){
        liveData.value = live
    }

    fun getLiveData() : LiveData<List<PixabayViewModel>>{
        liveData.value = viewModels
        return liveData
    }
    constructor()

    fun getViewModel(hits: List<Hits>): LinkedList<PixabayViewModel> {
        var list = LinkedList<PixabayViewModel>()
        val iterator = hits.listIterator()
        for (item in iterator) {
            list.add(PixabayViewModel(item))
        }
        return list
    }
    fun getHits(hits: PixabayViewModel): Hits {
        return Hits(hits)
    }
    companion object {
        @JvmStatic
        @BindingAdapter("android:src")
        fun loadImage(view: ImageView, imageUrl: String) {
            GlideApp.with(view)
                .load(imageUrl)
                .into(view)

        }
    }
    fun setFavoriteViewModel(viewModel : PixabayViewModel){
        favoriteViewModel.value = viewModel
    }
    fun getFavoriteViewModel() : LiveData<PixabayViewModel> {
        return favoriteViewModel
    }
}