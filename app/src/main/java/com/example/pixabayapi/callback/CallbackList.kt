package com.example.pixabayapi.callback

import com.example.pixabayapi.viewmodel.PixabayViewModel

interface CallbackList {
    fun clickStar(favoriteViewModel : PixabayViewModel)

}