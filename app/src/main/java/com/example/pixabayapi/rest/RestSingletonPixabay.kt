package com.example.pixabayapi.rest

import android.content.Context
import com.example.pixabayapi.BuildConfig
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RestSingletonPixabay {
    private var instance: RestSingletonPixabay? = null
    private var mContext: Context? = null
    private var url = "https://pixabay.com/api/"
    var api: ApiPixabay? = null

    companion object RestSingletonPixabay {

        val client = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor()
                .apply {
                    level = if (BuildConfig.DEBUG)
                        HttpLoggingInterceptor.Level.BODY
                    else
                        HttpLoggingInterceptor.Level.NONE
                })
            .build()
        private val url = "https://pixabay.com"

        fun create(): ApiPixabay? {
            val retrofit = Retrofit.Builder()
                .client(client)
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
            return retrofit.create(ApiPixabay::class.java)
        }
    }
}