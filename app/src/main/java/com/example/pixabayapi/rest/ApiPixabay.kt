package com.example.pixabayapi.rest

import com.example.pixabayapi.model.Results
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiPixabay {

    @GET("/api/?key=15211549-836e0aeb72a052a38de444c05")
    fun getResults(
        @Query("q") query: String,
        @Query("image_type") imageType: String
    ): Observable<Results>
}